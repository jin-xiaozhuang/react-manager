import resso from 'resso'
import { create } from 'zustand'

export const store = resso({
  userInfo: {
    userName: '',
    userId: 0
  }
})

export const useStore = create<{
  token: string
  collapsed: boolean
  updateCollapsed: () => void
  updateToken: (token: string) => void
}>(set => ({
  token: '',
  collapsed: false,
  updateCollapsed: () =>
    set(state => {
      return {
        collapsed: !state.collapsed
      }
    }),
  updateToken: token => set({ token })
}))
