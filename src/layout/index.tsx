import React, { useState, useEffect } from 'react'
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UploadOutlined,
  UserOutlined,
  VideoCameraOutlined
} from '@ant-design/icons'
import { Layout, Button, theme, Watermark, Switch, Dropdown } from 'antd'
import type { MenuProps } from 'antd'
import SideMenu from '@/components/menu'
import { Outlet } from 'react-router-dom'
import style from './index.module.less'
import api from '@/api/index'
import { store } from '@/store'
import storage from '@/utils/storage'
import { User } from '@/types/api'
import { useStore } from '@/store'

const { Header, Sider, Content, Footer } = Layout

const App: React.FC = () => {
  const { collapsed, updateCollapsed } = useStore()
  // useEffect(() => {
  //   // 水印无法删除实现
  //   const contentDom = document.getElementById('content') as HTMLDivElement
  //   const observer = new MutationObserver((mutationsList, observe) => {
  //     observe.disconnect()
  //     for (const mutation of mutationsList) {
  //       if (mutation.type === 'childList') {
  //         const span = document.createElement('span')
  //         span.innerHTML = 'content'
  //         contentDom.appendChild(span)
  //         observer.observe(contentDom, config)
  //       }
  //     }
  //   })
  //   const config = {
  //     attributes: true,
  //     childList: true,
  //     subtree: true
  //   }
  //   observer.observe(contentDom, config)
  // }, [])
  useEffect(() => {
    api.getUserInfo().then((res: User.UserItem) => {
      store.userInfo = res
    })
  }, [])

  const {
    token: { colorBgContainer }
  } = theme.useToken()
  const items: MenuProps['items'] = [
    {
      key: 'id',
      label: '用户id：' + store.userInfo.userId
    },
    {
      key: 'logout',
      label: '退出'
    }
  ]
  const onClick: MenuProps['onClick'] = ({ key }) => {
    if (key === 'logout') {
      storage.removeAccessToken()
      location.href = '/login?callabck=' + encodeURIComponent(location.href)
    }
  }

  return (
    <Watermark content='react'>
      <Layout>
        <Sider trigger={null} collapsible collapsed={collapsed}>
          <SideMenu />
        </Sider>
        <Layout>
          <Header className={style.header} style={{ padding: 0, background: colorBgContainer }}>
            <div>
              <Button
                type='text'
                icon={collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
                onClick={() => updateCollapsed()}
                style={{
                  fontSize: '16px',
                  width: 64,
                  height: 64
                }}
              />
              <span>首页 / 详情</span>
            </div>
            <div>
              <Switch checkedChildren='暗黑' unCheckedChildren='默认' />
              <Dropdown menu={{ items, onClick }} className={style.userInfo}>
                <span>{store.userInfo.userName}</span>
              </Dropdown>
            </div>
          </Header>
          <Content className={style.content}>
            <div className={style.wrapper}>
              <Outlet></Outlet>
            </div>
            <Footer style={{ textAlign: 'center' }}>Ant Design ©2023 Created by Ant UED</Footer>
          </Content>
        </Layout>
      </Layout>
    </Watermark>
  )
}

export default App
