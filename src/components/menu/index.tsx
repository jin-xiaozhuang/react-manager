import { Menu } from 'antd'
import {
  DesktopOutlined,
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
  ThunderboltTwoTone
} from '@ant-design/icons'
import style from './index.module.less'
import { useNavigate } from 'react-router-dom'
import { useStore } from '@/store'

const SideMenu = () => {
  const { collapsed } = useStore()
  const navigate = useNavigate()
  const items = [
    {
      key: '1',
      icon: <DesktopOutlined />,
      label: '工作台'
    },
    {
      key: '2',
      icon: <VideoCameraOutlined />,
      label: '系统管理',
      children: [
        {
          key: '4',
          icon: <UserOutlined />,
          label: '用户管理'
        }
      ]
    },
    {
      key: '3',
      icon: <UploadOutlined />,
      label: '下载管理'
    }
  ]
  const clickLogo = () => {
    navigate('/welcome')
  }
  return (
    <div>
      <div className={style.logo} onClick={clickLogo}>
        <ThunderboltTwoTone className={style.icon} />
        <span>{collapsed ? '' : '滴滴货运'}</span>
      </div>
      <Menu theme='dark' mode='inline' defaultSelectedKeys={['1']} items={items} />
    </div>
  )
}

export default SideMenu
