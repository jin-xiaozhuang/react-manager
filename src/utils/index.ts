// 工具函数封装

export const formatMoney = (num: string | number) => {
  const amount = parseFloat(num.toString())
  return amount.toLocaleString('zh-CN', { style: 'currency', currency: 'CNY' })
}

export const formatDate = (date?: Date, rule?: string) => {
  let curDate = date ? date : new Date()
  if (rule === 'yyy-MM-dd') return curDate.toLocaleDateString().replaceAll('/', '-')
  if (rule === 'HH:mm:ss') return curDate.toLocaleTimeString().replaceAll('/', '-')
  return curDate.toLocaleString().replaceAll('/', '-')
}
