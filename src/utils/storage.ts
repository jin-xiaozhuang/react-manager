import jsCookie from 'js-cookie'
export default {
  set(key: string, value: any) {
    localStorage.setItem(key, JSON.stringify(value))
  },
  get(key: string) {
    const value = localStorage.getItem(key)
    if (!value) return ''
    try {
      return JSON.parse(value)
    } catch (error) {
      return value
    }
  },
  remove(key: string) {
    localStorage.removeItem(key)
  },
  clear() {
    localStorage.clear()
  },
  removeAccessToken() {
    jsCookie.remove('access_token')
  },
  getAccessToken() {
    return jsCookie.get('access_token')
  },
  setAccessToken(value: string) {
    jsCookie.set('access_token', value, { expires: 0.5 })
  }
}
