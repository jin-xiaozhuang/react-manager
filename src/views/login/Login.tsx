import { Button, Form, Input } from 'antd'
import style from '@/views/login/index.module.less'
import { useNavigate } from 'react-router-dom'
import api from '@/api/index'
import storage from '@/utils/storage'
import { useStore } from '@/store'

function Login() {
  const updateToken = useStore(state => state.updateToken)
  const onFinish = async (values: any) => {
    const res = await api.login(values)
    storage.setAccessToken(res)
    updateToken(res)
    const callback = new URLSearchParams(location.search)
    location.href = callback.get('callback') || '/welcome'
  }

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo)
  }
  return (
    <div className={style.login}>
      <div className={style.loginWarpper}>
        <div className={style.title}>系统登录</div>
        <Form
          name='basic'
          style={{ maxWidth: 600 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete='off'
        >
          <Form.Item name='username' rules={[{ required: true, message: 'Please input your username!' }]}>
            <Input />
          </Form.Item>

          <Form.Item name='password' rules={[{ required: true, message: 'Please input your password!' }]}>
            <Input.Password />
          </Form.Item>

          <Form.Item>
            <Button type='primary' htmlType='submit' block>
              登录
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  )
}

export default Login
