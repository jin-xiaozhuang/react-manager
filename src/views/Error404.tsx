import { Result, Button } from 'antd'
import { useNavigate } from 'react-router-dom'

function Error404() {
  const navigate = useNavigate()
  const back = () => {
    navigate('/')
  }
  return (
    <Result
      status='404'
      title='404'
      subTitle='抱歉，您没有权限查看此页面。'
      extra={
        <Button type='primary' onClick={back}>
          回首页
        </Button>
      }
    />
  )
}

export default Error404
